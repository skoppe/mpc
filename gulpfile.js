var gulp = require('gulp');
var watchify = require('watchify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var rev = require('gulp-rev');
var babelify = require("babelify");
var gutil = require('gulp-util');
var uglify = require('gulp-uglify');
var bundle = require('gulp-bundle-assets');
var through = require('through2');
var fs = require('fs');

function storeScripts(output)
{
	return function(file, enc, cb)
	{
		fs.writeFile(output, file.relative);
		cb();
	}
}
function createBundler(debug,doWatch)
{
	if (doWatch)
		var args = watchify.args;
	else
		var args = {};
	args.entries = "./source/index.js";
	args.debug = debug;
	if (doWatch)
		var bundler = watchify(browserify(args));
	else
		var bundler = browserify(args);
	return bundler.transform(babelify.configure({ modules: "common", only: /source\// }));
}
function bundleJs(bundler,dest,name,compress)
{
	compress = !!compress;
	name = name || 'index.js';
	dest = dest || './public/assets/js';
	bundler = bundler || createBundler(false,false);
	var output = bundler
		.bundle().on('error',function(err){console.log(err);gutil.log(err);this.emit("end");})
		.pipe(source(name))
		.pipe(buffer());
	if (compress == true)
		output = output.pipe(uglify());
	return output.pipe(rev()).pipe(gulp.dest(dest)).pipe(through.obj(storeScripts('./bundle.js.conf')));
}
gulp.task('js', function js()
{
	var bundler = createBundler(false,false);
	return bundleJs(bundler);
});
gulp.task('js-release', function js()
{
	var bundler = createBundler(false,false);
	return bundleJs(bundler,undefined,undefined,true);
});
gulp.task('watch-js', function watch()
{
	var bundler = createBundler(false,true);
	bundler.on('update', function(){ return bundleJs(bundler)});
	bundler.on('log', gutil.log);
	return bundleJs(bundler);
});
gulp.task('less', function() {
	return gulp.src('./bundle.css.js')
		.pipe(bundle())
		.pipe(bundle.results({dest:'./',pathPrefix:'/'}))
		.pipe(gulp.dest('./public/assets/css/'))
		.pipe(through.obj(storeScripts('./bundle.css.conf')));
});
gulp.task('release',['js-release','less']);
gulp.task('watch',['watch-js'],function()
{
	return gulp.watch(['source/less/**/*'],['less']);	
});