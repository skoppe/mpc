import React from 'react';
import { Rx } from 'rx';
import { Router, Route, Link, Navigation, IndexRoute } from 'react-router';
import createBrowserHistory from 'history/lib/createBrowserHistory';
import AlbumStore from './stores/album-store.js';
import AlbumAction from './actions/album-action.js';
import ControlAction from './actions/control-action.js';
import StatusStore from './stores/status-store.js';
import ChangeStore from './stores/change-store.js';
import PlaylistStore from './stores/playlist-store.js';
import GenreStore from './stores/genre-store.js';
import ArtistStore from './stores/artist-store.js';
import {saveState} from 'history/lib/DOMStateStorage';

// console.log(saveState);
let history = createBrowserHistory();
let params = {};
let path = "/";
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };
function isModifiedEvent(event) {
  return !!(event.metaKey || event.altKey || event.ctrlKey || event.shiftKey);
}
function isLeftClickEvent(event) {
  return event.button === 0;
}
function _objectWithoutProperties(obj, keys) { var target = {}; for (var i in obj) { if (keys.indexOf(i) >= 0) continue; if (!Object.prototype.hasOwnProperty.call(obj, i)) continue; target[i] = obj[i]; } return target; }

class LiLink extends React.Component{
	constructor(props,context){
		super(props,context);
	}
	static getDefaultProps: {
		activeClassName: 'active',
		onlyActiveOnIndex: false,
		className: '',
		style: {}
	}

	handleClick(event) {
		var allowTransition = true,
		    clickResult = undefined;

		if (this.props.onClick) clickResult = this.props.onClick(event);

		if (isModifiedEvent(event) || !isLeftClickEvent(event)) return;

		if (clickResult === false || event.defaultPrevented === true) allowTransition = false;

		event.preventDefault();

		if (allowTransition) this.context.history.pushState(this.props.state, this.props.to, this.props.query);
	}
	render() {

		var _this = this;

		var _props = this.props;
		var to = _props.to;
		var query = _props.query;
		var hash = _props.hash;
		var state = _props.state;
		var activeClassName = _props.activeClassName || 'active';
		var activeStyle = _props.activeStyle;
		var onlyActiveOnIndex = _props.onlyActiveOnIndex || false;

		var props = _objectWithoutProperties(_props, ['to', 'query', 'hash', 'state', 'activeClassName', 'activeStyle', 'onlyActiveOnIndex']);

		// Manually override onClick.
		props.onClick = function (e) {
		  return _this.handleClick(e);
		};
		let liProps = { className: "", style: {}};

		// Ignore if rendered outside the context of history, simplifies unit testing.
		var history = this.context.history;
		if (history) {
		  props.href = history.createHref(to, query);

		  if (hash) props.href += hash;


		  if (activeClassName || activeStyle != null && !isEmptyObject(activeStyle)) {
		    if (history.isActive(to, query, onlyActiveOnIndex)) {
		      if (activeClassName) liProps.className += liProps.className === '' ? activeClassName : ' ' + activeClassName;

		      if (activeStyle) liProps.style = _extends({}, liProps.style, activeStyle);
		    }
		  }
		}
		return (
			<li {...liProps}><a {...props}>{this.props.children}</a></li>
		)
	}
}
LiLink.displayName = 'LiLink';
LiLink.contextTypes = {
	history: React.PropTypes.object
}
LiLink.propTypes = {
	to: React.PropTypes.string.isRequired,
	query: React.PropTypes.object,
	hash: React.PropTypes.string,
	state: React.PropTypes.object,
	activeStyle: React.PropTypes.object,
	activeClassName: React.PropTypes.string,
	onlyActiveOnIndex: React.PropTypes.bool,
	onClick: React.PropTypes.func
}
LiLink.defaultProps= {
	activeClassName: 'active',
	onlyActiveOnIndex: false,
	className: '',
	style: {}
}
function appendPath(extra)
{
	let paths = path.split("/");
	let extras = extra.split("/");
	if (paths[paths.length-1] == extras[0])
		extras.shift();
	return paths.concat(extras).join("/");
}
class Albums extends React.Component
{
	constructor(props,context){
		super(props,context);
	}
	componentWillMount(){
		this.state = getState(this,{search: ""});
	}
	_onSearch(event){
		persistState(this,{search:event.currentTarget.value.toLowerCase()});
	}
	render(){
		let albums;
		if (this.state.search != "")
			albums = this.props.albums.filter(album=>album.name.toLowerCase().indexOf(this.state.search) != -1);
		else
			albums = this.props.albums;
		return (
			<div className="panel panel-default">
				<div className="panel-body">
					<div className="form-group">
						<input type="text" value={this.state.search} className="form-control" onChange={e=>this._onSearch(e)} placeholder="Search" />
					</div>
					<div className="row">
						{
							albums.map(album=>{
								return (
									<div key={album.name} className="col-xs-6">
										<Link state={{bla:"Blala"}} to={appendPath("album/"+encodeURIComponent(album.name))}>
											<button type="button" className="btn btn-default btn-lg btn-block">{album.name}</button>
										</Link>
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		)
	}
}
class AlbumsContainer extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
		AlbumStore.getAlbums(params).subscribe(albums=>this.setState({albums}));
	}
	render(){
		if (this.state.albums)
			return <Albums albums={this.state.albums} children={this.props.children}/>
		return null;
	}
}
class ArtistDetailContainer extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
		ArtistStore.getArtist(props.params.artist).subscribe(albums=>this.setState({albums}));
	}
	render(){
		if (this.state.albums)
			return <Albums albums={this.state.albums} children={this.props.children}/>
		return null;
	}
}
class Genres extends React.Component
{
	constructor(props,context){
		super(props,context);
	}
	componentWillMount(){
		this.state = getState(this,{search: ""});
	}
	_onSearch(event){
		persistState(this,{search:event.currentTarget.value.toLowerCase()});
	}
	render(){
		let genres;
		if (this.state.search != "")
			genres = this.props.genres.filter(album=>album.name.toLowerCase().indexOf(this.state.search) != -1);
		else
			genres = this.props.genres;
		return (
			<div className="panel panel-default">
				<div className="panel-body">
					<div className="form-group">
						<input type="text" value={this.state.search} className="form-control" onChange={e=>this._onSearch(e)} placeholder="Search" />
					</div>
					<div className="row">
						{
							genres.map(genre=>{
								return (
									<div key={genre.name} className="col-xs-6">
										<Link to={"/genre/"+encodeURIComponent(genre.name)}>
											<button type="button" className="btn btn-default btn-lg btn-block">{genre.name}</button>
										</Link>
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		)
	}
}
class GenreContainer extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
		GenreStore.getGenres().subscribe(genres=>this.setState({genres}));
	}
	render(){
		if (this.state.genres)
			return <Genres genres={this.state.genres} children={this.props.children}/>
		return null;
	}
}
class GenreDetail extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
	}
	render(){
		return (
			<div>
				{this.props.children}
			</div>
		)
	}
}
class Artists extends React.Component
{
	constructor(props,context){
		super(props,context);
	}
	componentWillMount(){
		this.state = getState(this,{search: ""});
	}
	_onSearch(event){
		persistState(this,{search:event.currentTarget.value.toLowerCase()});
	}
	render(){
		let artists;
		if (this.state.search != "")
			artists = this.props.artists.filter(album=>album.name.toLowerCase().indexOf(this.state.search) != -1);
		else
			artists = this.props.artists;
		return (
			<div className="panel panel-default">
				<div className="panel-body">
					<div className="form-group">
						<input type="text" value={this.state.search} className="form-control" onChange={e=>this._onSearch(e)} placeholder="Search" />
					</div>
					<div className="row">
						{
							artists.map(artist=>{
								return (
									<div key={artist.name} className="col-xs-6">
										<Link to={appendPath("artist/"+encodeURIComponent(artist.name))}>
											<button type="button" className="btn btn-default btn-lg btn-block">{artist.name}</button>
										</Link>
									</div>
								)
							})
						}
					</div>
				</div>
			</div>
		)
	}
}
class ArtistsContainer extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
		ArtistStore.getArtists(params).subscribe(artists=>this.setState({artists}));
	}
	render(){
		if (this.state.artists)
			return <Artists artists={this.state.artists} children={this.props.children}/>
		return null;
	}
}
class AlbumDetail extends React.Component
{
	render(){
		return (
			<div className="panel panel-default">
				<div className="panel-heading">{this.props.songs[0].album} - {this.props.songs[0].artist}</div>
				<div className="panel-body">
					<button className="btn btn-primary" onClick={e=>this.props.action("play")}><i className="fa fa-play"/> Play All</button>
					<button className="btn btn-secondary" onClick={e=>this.props.action("enqueue")}><i className="fa fa-list"/> Enqueue</button>
					<div className="">
						<table className="table table-striped">
							<thead>
								<th>Track</th>
								<th>Title</th>
							</thead>
							<tbody>
								{
									this.props.songs.map((song,idx)=>{
										return (
											<tr key={idx}>
												<td>{song.track}.</td>
												<td>{song.title}</td>
											</tr>
										)
									})
								}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		)
	}
}
class AlbumDetailContainer extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
		AlbumStore.getAlbum(props.routeParams.album).subscribe(songs=>this.setState({songs}));
	}
	action(type,...args){
		switch (type)
		{
			case "play":
			{
				AlbumAction.play(this.props.routeParams.album);
			} break;
			case "enqueue":
			{
				AlbumAction.enqueue(this.props.routeParams.album);
			} break;
		}
	}
	render(){
		if (this.state.songs)
			return <AlbumDetail songs={this.state.songs} children={this.props.children} action={(...args)=>this.action(...args)}/>
		return null;
	}
}
class PlayList extends React.Component
{
	render(){
		return (
			<div className="panel panel-default">
				<div className="panel-heading">Playlist</div>
				<div className="panel-body">
					<div className="">
						<table className="table table-striped">
							<thead>
								<th></th>
								<th>Artist</th>
								<th>Album</th>
								<th>Title</th>
							</thead>
							<tbody>
								{
									this.props.playlist.map(song=>{
										let active = this.props.status.status.song == song.pos;
										let cls = "";
										if (active)
											cls = "info";
										return (
											<tr key={song.pos} className={cls}>
												<td className="hover-show">
													<button className="btn btn-small btn-primary" onClick={e=>this.props.action("play",song.pos)}><i className="fa fa-play"/></button>
												</td>
												<td>{song.artist}</td>
												<td>{song.album}</td>
												<td>{song.title}</td>
											</tr>
										)
									})
								}
							</tbody>
						</table>
					</div>
				</div>
			</div>
		)
	}
}
class PlaylistContainer extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
	}
	componentDidMount(){
		Rx.Observable.combineLatest(
			StatusStore,
			PlaylistStore,
			(status,playlist)=>{
				this.setState({status,playlist});
			}
		).subscribe();
	}
	action(type,...args){
		console.log(type,...args);
		switch (type)
		{
			case "play":
			{
				ControlAction.play(...args).subscribe();
			} break;
		}
	}
	render(){
		if (this.state.status && this.state.playlist)
			return <PlayList playlist={this.state.playlist} status={this.state.status} children={this.props.children} action={(...args)=>this.action(...args)}/>
		return null;
	}
}
class SideMenu extends React.Component
{
	render(){
		return (
			<div className="panel panel-default">
				<div className="list-group">
					<Link to="/playlist" activeClassName="active" className="list-group-item">
						Playlists
					</Link>
					<Link to="/album" activeClassName="active" className="list-group-item">
						Albums
					</Link>
					<Link to="/genre" activeClassName="active" className="list-group-item">
						Genres
					</Link>
					<Link to="/artist" activeClassName="active" className="list-group-item">
						Artists
					</Link>
				</div>
			</div>
		)
	}
}
class Icon extends React.Component
{
	render(){
		let clx = ["fa","fa-"+this.props.icon];
		if (this.props.disabled)
			clx.push("disabled");
		return (
			<i className={clx.join(" ")} />
		);
	}
}
class Button extends React.Component
{
	render(){
		let cls = "btn btn-"+this.props.btn;
		if (this.props.disabled)
			cls += " disabled";
		return (
			<button type="button" className={cls} onClick={this.props.onClick}>{this.props.children}</button>
		)
	}
}
class Control extends React.Component
{
	render(){
		let cls = "default";
		if (this.props.highlight)
			cls = "primary"
		return (
			<Button btn={cls} onClick={this.props.onClick}>{this.props.children}</Button>
		)
	}
}
class Controls extends React.Component
{
	render(){
		let repeatCls;
		return (
			<div className="panel panel-default">
				<div className="panel-body">
					<center>
						<div className="btn-group" role="group" aria-label="...">
							<Button btn="default" onClick={e=>this.props.action("previous")}><i className="fa fa-backward"/></Button>
							<Button btn="default" onClick={e=>this.props.action("play")}><i className="fa fa-play"/></Button>
							<Button btn="default" onClick={e=>this.props.action("stop")}><i className="fa fa-stop"/></Button>
							<Button btn="default" onClick={e=>this.props.action("next")}><i className="fa fa-forward"/></Button>
							<Button btn="default" disabled={this.props.status.status.volume == 100} onClick={e=>this.props.action("plus")}><i className="fa fa-plus"/></Button>
							<Button btn="default" disabled={this.props.status.status.volume == 0} onClick={e=>this.props.action("minus")}><i className="fa fa-minus"/></Button>
							<Control highlight={this.props.status.status.repeat == 1} onClick={e=>this.props.action("repeat")}><Icon icon="repeat" /></Control>
							<Control highlight={this.props.status.status.random == 1} onClick={e=>this.props.action("random")}><Icon icon="random" /></Control>
						</div>
					</center>
				</div>
			</div>
		)
	}
}
class ControlsContainer extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
		StatusStore.subscribe(status=>this.setState({status}));
	}
	action(type,...args){
		switch (type)
		{
			case "plus": ControlAction.volume.set(this.state.status.status.volume + 5).subscribe(); break;
			case "minus": ControlAction.volume.set(this.state.status.status.volume - 5).subscribe(); break;
			case "previous": ControlAction.previous().subscribe(); break;
			case "play": 
				if (this.state.status.status.song == -1)
					ControlAction.play().subscribe();
				else
					ControlAction.resume().subscribe();
				break;
			case "stop": ControlAction.pause().subscribe(); break;
			case "next": ControlAction.next().subscribe(); break;
			case "repeat": ControlAction.repeat(this.state.status.status.repeat == 0).subscribe(); break;
			case "random": ControlAction.random(this.state.status.status.random == 0).subscribe(); break;
		}
	}
	render(){
		if (!this.state.status)
			return null;
		return <Controls status={this.state.status} action={(...args)=>this.action(...args)}/>
	}
}
class PlayListInfo extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
		StatusStore.subscribe(status=>this.setState({status}));
	}
	render(){
		if (!this.state.status)
			return null;
		return (
			<div className="panel panel-default">
				<div className="panel-heading">Current Song</div>
				<div className="panel-body">
					<div>
						<div><span className="text-gray-light">Artist:</span> {this.state.status.current.artist}</div>
						<div><span className="text-gray-light">Album:</span> {this.state.status.current.album}</div>
						<div><span className="text-gray-light">Title:</span> {this.state.status.current.title}</div>
					</div>
				</div>
			</div>
		)
	}	
}
class ServerInfo extends React.Component
{
	constructor(props,context){
		super(props,context);
		this.state = {};
	}
	componentWillMount(){
		ChangeStore.status.subscribe((connected)=>this.setState({connected}));
	}
	render(){
		let cls = "panel panel-";
		if (this.state.connected)
			cls += "success";
		else
			cls += "danger";
		return (
			<div className={cls}>
				<div className="panel-body">
					<p className={ this.state.connected ? "" : "text-danger"}>
						{
							this.state.connected ? "Online" : "Offline"
						}
					</p>
				</div>
			</div>
		)
	}
}
function createBreadcrumbs(params)
{
	let crumbs = [{caption:"Home",link:""}];
	if (params.genre)
		crumbs.push({caption:params.genre,link:"/genre/"+params.genre})
	if (params.artist)
		crumbs.push({caption:params.artist,link:"/artist/"+params.artist})
	if (params.album)
		crumbs.push({caption:params.album,link:"/album/"+params.album})
	let prefix = "";
	return crumbs.map(crumb=>{
		crumb.link = prefix + crumb.link;
		prefix = crumb.link;
		return crumb;
	});
}
class App extends React.Component
{
	render(){
		params = this.props.params;
		path = this.props.location.pathname;
		crumbs = prepareCrumbs(crumbs);
		return (
			<div className="container-fluid">
				<div className="row">
					<div className="col-xs-4 col-sm-3 col-md-2">
						<SideMenu />
						<ControlsContainer />
						<PlayListInfo />
						<ServerInfo />
					</div>
					<div className="col-xs-8 col-sm-9 col-md-10">
						<div>
							<ol className="breadcrumb">
								{
									crumbs.map((crumb,idx)=>{
										if (idx == crumbs.length-1)
											return (<li key={crumb.caption} className="active">{crumb.caption}</li>)
										return (
											<LiLink key={crumb.caption} to={crumb.link}>{crumb.caption}</LiLink>
										);
									})
								}
							</ol>
						</div>
						{this.props.children}
					</div>
				</div>
			</div>
		)
	}
}
App.contextTypes = {
  router: React.PropTypes.object
}
class Dummy
{
	render(){
		return (<div>{this.props.children}</div>);
	}
}
let location;
function setLocation(loc)
{
	location = loc;
}
function persistState(component,state)
{
	let id = component._reactInternalInstance._rootNodeID;
	location.state = location.state || {};
	location.state[id] = location.state[id] || {};
	location.state[id] = _extends({},location.state[id],state);
	saveState(location.key, location.state);
	component.setState(state);
}
function getState(component, def)
{
	let id = component._reactInternalInstance._rootNodeID;
	return location.state ? (location.state[id] || def) : def;
}
let crumbs = [];
function createCrumb(props)
{
	switch (props.route.path)
	{
		case ":genre": return {caption:props.params.genre,link:"/"+props.params.genre};
		case "/genre": return {caption:"Genres",link:"/genre"};
		case ":artist": return {caption:props.params.artist,link:"/"+props.params.artist};
		case "/artist": return {caption:"Artists",link:"/artist"};
		case "artist/:artist": return {caption:props.params.artist,link:"/artist/"+props.params.artist};
		case ":album": return {caption:props.params.album,link:"/"+props.params.album};
		case "/album": return {caption:"Albums",link:"/album"};
		case "album/:album": return {caption:props.params.album,link:"/album/"+props.params.album};
		case "/": return {caption:"Home",link:"/"};
	}
}
function createElement(Component, props)
{
	setLocation(props.location);
	let c = createCrumb(props);
	if (c)
		crumbs.unshift(c);
	return (<Component {...props}/>);
}
function flushCrumbs(){
	crumbs = [];
}
function prepareCrumbs(){
	let prefix = "";
	return crumbs.map(crumb=>{
		crumb.link = prefix + crumb.link;
		prefix = crumb.link;
		if (prefix == "/")
			prefix = "";
		return crumb;
	})
}
React.render((
	<Router history={history} createElement={createElement} onUpdate={flushCrumbs}>
		<Route path="/" component={App}>
			<Route path="/album" component={Dummy} >
				<IndexRoute component={AlbumsContainer} />
				<Route path=":album" component={AlbumDetailContainer} />
			</Route>
			<Route path="/genre" component={Dummy} >
				<IndexRoute component={GenreContainer} />
				<Route path=":genre" component={GenreDetail} >
					<IndexRoute component={ArtistsContainer} />
					<Route path="artist/:artist" component={Dummy} >
						<IndexRoute	component={ArtistDetailContainer} />
						<Route path="album/:album" component={AlbumDetailContainer} />
					</Route>
				</Route>
			</Route>
			<Route path="/artist" component={Dummy} >
				<IndexRoute component={ArtistsContainer} />
				<Route path=":artist" component={Dummy}>
					<IndexRoute component={ArtistDetailContainer} />
					<Route path="album/:album" component={AlbumDetailContainer} />
				</Route>
			</Route>
			<Route path="/playlist" component={PlaylistContainer} />
		</Route>
	</Router>
), document.querySelector('[data-role="app"]'));