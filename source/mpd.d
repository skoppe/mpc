module mpcd.mpd;

import mpcd.socket;
import vibe.core.core : sleep, Tid, runTask;
import std.datetime : msecs;
import std.exception : enforce;
import vibe.core.sync : TaskMutex;
import observe;

class DaemonError : Exception
{
	int code;
	string command; // the command that caused the exception
	this(int c, string cmd, string msg)
	{
		code = c;
		command = cmd;
		super(msg);	
	}
}
struct Name
{
	string name;
}
struct Delimiter
{
	string delimiter;
}
enum StatusChanged
{
	database, 			// the song database has been modified after update.
	update, 			// a database update has started or finished. If the database was modified during the update, the database event is also emitted.
	stored_playlist, 	// a stored playlist has been modified, renamed, created or deleted
	playlist, 			// the current playlist has been modified
	player, 			// the player has been started, stopped or seeked
	mixer, 				// the volume has been changed
	output, 			// an audio output has been enabled or disabled
	options, 			// options like repeat, random, crossfade, replay gain
	sticker, 			// the sticker database has been modified.
	subscription, 		// a client has subscribed or unsubscribed to a channel
	message 			// a message was received on a channel this client is subscribed to; this event is only emitted when the queue is empty
}
StatusChanged parseStatus(R)(R string)
{
	import std.algorithm : skipOver, until, map, countUntil;
	import std.conv : to;
	import std.conv : text;
	enforce(string.skipOver("changed: "));
	auto len = string.countUntil("\n");
	if (len == -1)
		return string.to!StatusChanged;
	return string[0..len].to!StatusChanged;
}
unittest
{
	assert("changed: database".parseStatus == StatusChanged.database);
	assert("changed: update\n".parseStatus == StatusChanged.update);
	assert("changed: stored_playlist\n  ".parseStatus == StatusChanged.stored_playlist);
	assert("changed: playlist\n  df".parseStatus == StatusChanged.playlist);
	assert("changed: player\n\n".parseStatus == StatusChanged.player);
	assert("changed: mixer\nplayer\n".parseStatus == StatusChanged.mixer);
	assert("changed: output".parseStatus == StatusChanged.output);
	assert("changed: options".parseStatus == StatusChanged.options);
	assert("changed: sticker".parseStatus == StatusChanged.sticker);
	assert("changed: subscription".parseStatus == StatusChanged.subscription);
	assert("changed: message".parseStatus == StatusChanged.message);
}
class DaemonStatusSubject
{
	private
	{
		Daemon daemon;
		TaskMutex mutex;
		Tid tid;
		Subject!(StatusChanged) subject;
		uint subs = 0;
	}
	this(Daemon d, TaskMutex m)
	{
		daemon = d;
		mutex = m;
		subject = new Subject!(StatusChanged);
	}
	private void start()
	{
		mutex.lock();
		tid = runTask({
			import std.stdio;
			try
			{
				daemon.send("idle");
			} catch (Exception e)
			{
				subject.onError(e);
				return;
			}
			bool running = true;
			do
			{
				import core.time : dur;
				if (daemon.waitForData(dur!"msecs"(500)))
				{
					char[] r = daemon.read();
					r.throwOnError();
					subject.onNext(r.parseStatus);
					try
					{
						daemon.send("idle");
					} catch (Exception e)
					{
						subject.onError(e);
						return;
					}
				}
				import vibe.core.concurrency : receiveTimeout;
				receiveTimeout(dur!"msecs"(100),(bool error){
					writeln("receiveTimeout");
					if (!error)
						daemon.send("unidle");
					running = false;
				});
			} while (running);
			writeln("task end");
		});
	}
	private void end(bool error = false)
	{
		import vibe.core.concurrency : send;
		writeln(tid);
		tid.send(error);
		tid.join();
		mutex.unlock();
		writeln("ended");
	}
	private void subscribed()
	{
		subs++;
		if (subs == 1)
			start();
	}
	private void disposed()
	{
		subs--;
		if (subs == 0)
			end();
	}
	private struct Subscription
	{
		private{
			Subject!(StatusChanged).Sub sub;
			bool active = true;
			DaemonStatusSubject dss;
		}
		void dispose()
		{
			writeln("in dispose");
			if (active)
			{
				active = false;
				writeln("disposed");
				writeln(dss);
				dss.disposed();
				writeln(sub);
				sub.dispose();
				writeln("dispose");
			}
		}
	}
	auto subscribe(Subject!(StatusChanged).NextFunc onNext = null, ErrorFunc onError = null, CompletedFunc onCompleted = null)
	{
		auto sub = subject.subscribe(onNext,onError,onCompleted);
		subscribed();
		return Subscription(sub,true,this);
	}
}
void throwOnError(char[] r)
{
	if (r[0..3] == "ACK")
	{
		import std.conv : to;
		import std.regex;
		auto reg = ctRegex!`ACK \[([0-9]+)@[0-9]+\] \{([^}]*)\} ([^\n]+)\n`;
		auto matches = r.matchFirst(reg);
		if (matches.empty)
			throw new DaemonError(0,"","Unknown error");
		throw new DaemonError(matches[1].to!int,matches[2].to!string,matches[3].to!string);
	}
}
class Daemon
{
	private {
		Socket soc;
		string ver;
		string host;
		ushort port;
		TaskMutex mutex;
		DaemonStatusSubject subject;
		void open()
		{
			import std.algorithm : until, splitter;
			import std.range : drop, front, array;
			import std.array : split;
			import std.conv : to;
			import std.stdio;
			soc.open(host,port);
			ver = soc.read.until('\n').array.splitter(' ').drop(2).front().to!string;
		}
		bool hasData()
		{
			return soc.hasData();
		}
		char[] read()
		{
			return soc.read();
		}
		auto send(Range)(Range command)
		{
			import std.conv : to;
			import std.range : chain;
			try
			{
				soc.send(command.chain("\n").to!(char[]));
				return;
			} catch (Exception e)
			{
			}
			// probably the socket was closed on the otherside, try again, this time no catching of exceptions
			open();
			soc.send(command.chain("\n").to!(char[]));
		}
		bool waitForData(core.time.Duration d)
		{
			return soc.waitForData(d);
		}
	}
	this(string h, ushort p)
	{
		host = h;
		port = p;
		open();
		mutex = new TaskMutex();
		subject = new DaemonStatusSubject(this,mutex);
	}
	void close()
	{
		soc.close();
	}
	string model()
	{
		return ver;
	}
	char[] command(Range)(Range command)
	{
		import std.conv : to;
		import std.range : chain;
		mutex.lock();
		scope(exit)mutex.unlock();
		send(command);
		char[] r = soc.read();
		r.throwOnError();
		if (r[$-3..$] == "OK\n")
			return r[0..$-3];
		import std.array : appender;
		auto a = appender!(char[]);
		a.put(r);
		do
		{
			r = soc.read();
			a.put(r);
		} while (a.data[$-3..$] != "OK\n");
		return a.data[0..$-3];
	}
	auto idle()
	{
		return subject;
	}
}
private template getUDA(alias Symbol, alias UDA)
{
	import std.traits : isInstanceOf;
	import std.typetuple : TypeTuple;

	private alias udaTuple = TypeTuple!(__traits(getAttributes, Symbol));
	public template extract(size_t index, list...)
	{
		static if (!list.length) static assert("Symbol "~__traits(identifier,Symbol)~" has got no UDA "~__traits(identifier,UDA));
		else {
			static if (is(list[0])) {
				static if (is(UDA) && is(list[0] == UDA) || !is(UDA) && isInstanceOf!(UDA, list[0])) {
					enum extract = list[0];
				} else enum extract = extract!(index + 1, list[1..$]);
			} else {
				static if (is(UDA) && is(typeof(list[0]) == UDA) || !is(UDA) && isInstanceOf!(UDA, typeof(list[0]))) {
					enum extract = list[0];
				} else enum extract = extract!(index + 1, list[1..$]);
			}
		}
	}
	static const size_t index = 0;
	enum getUDA = extract!(index, udaTuple[0..$]);
}
private template hasUDA(alias symbol, alias attribute)
{
    import std.typetuple : staticIndexOf;
    import std.traits : staticMap;

    static if (is(attribute == struct) || is(attribute == class))
    {
        template GetTypeOrExp(alias S)
        {
            static if (is(typeof(S)))
                alias GetTypeOrExp = typeof(S);
            else
                alias GetTypeOrExp = S;
        }
        enum bool hasUDA = staticIndexOf!(attribute, staticMap!(GetTypeOrExp,
                __traits(getAttributes, symbol))) != -1;
    }
    else
        enum bool hasUDA = staticIndexOf!(attribute, __traits(getAttributes, symbol)) != -1;
}
auto parse(char[] data)
{
	import std.algorithm : splitter, map, findSplit;
	return data.splitter('\n').map!(l=>l.findSplit(": "));
}
auto deserialize(Target, Range)(Range data)
	if (is(Range : char[]))
{
	return deserialize!Target(data.parse);
}
auto deserialize(E : E[], Range)(Range data)
	if (!is(Range : char[]))
{
	import std.algorithm : countUntil;
	import std.range : array;
	static if (!hasUDA!(E,Delimiter))
		static assert (false, "Deserializing an array requires a Delimiter attribute defined on the ElementType");
	enum delimiter = getUDA!(E,Delimiter).delimiter;
	E[] dst;
	auto arr = data.array();
	while (arr.length > 0)
	{
		size_t end = arr[1..$].countUntil!((a){
			return a[0] == delimiter;
		}) + 1;
		if (end == 0)
		{
			dst ~= arr[0..$].deserialize!E;
			break;
		}
		dst ~= arr[0..end].deserialize!E;
		arr = arr[end..$];
	}
	return dst;
}
auto deserialize(Target, Range)(Range data)
	if (!is(Range : char[]) &&
		!is(Target ElementType : ElementType[]))
{
	import std.traits : isSomeString;
	import std.conv : to;
	Target dst;
	foreach(t; data)
	{
		switch(t[0])
		{
			default: break;
			foreach (mname; __traits(allMembers, Target)) {
				alias Type = typeof(__traits(getMember, dst, mname));
				static if (hasUDA!(__traits(getMember, dst, mname),Name))
					enum name = getUDA!(__traits(getMember, dst, mname),Name).name;
				else 
					enum name = mname;
				case name:
					static if (!isSomeString!Type && is(Type SubType: SubType[]))
					{
						__traits(getMember, dst, mname) ~= t[2].to!SubType;
					} else
						__traits(getMember, dst, mname) = t[2].to!Type;
				break;
			}
		}
	}
	return dst;
}

enum State
{
	play,
	stop,
	pause
}

struct Status
{
	int volume;
	int repeat;
	int random;
	int single;
	int consume;
	int playlist;
	int playlistlength;
	double mixrampdb = 0.0000;
	State state;
	int xfade;
	int song = -1;
	int songid = -1;
	int nextsong = -1;
	int nextsongid = -1;
	string time;
	double elapsed = 0.0000;
	int duration;
	double bitrate = 0.0000;
	double mixrampdelay = 0.0000;
	string audio;
	string updating_db;
	string error;
}
auto status(Daemon d)
{
	return d.command("status").deserialize!Status;
}
struct Stats
{
	int artists;
	int albums;
	int songs;
	long uptime;
	long db_playtime;
	long db_update;
	long playtime;
}
auto stats(Daemon d)
{
	return d.command("stats").deserialize!Stats;
}
@Delimiter("file") struct Song
{
	string file;
	@Name("Last-Modified") string lastModified;
	@Name("Title") string title;
	@Name("Artist") string artist;
	@Name("Composer") string[] composer;
	@Name("Album") string album;
	@Name("Genre") string genre;
	@Name("Date") string date;
	@Name("AlbumArtist") string albumArtist;
	@Name("Track") string track;
	@Name("Disc") string disc;
	@Name("Pos") int pos;
	@Name("Id") int id;
}
auto currentSong(Daemon d)
{
	return d.command("currentsong").deserialize!Song;
}
void next(Daemon d)
{
	d.command("next");
}
void previous(Daemon d)
{
	d.command("previous");
}
void play(Daemon d, int pos = 0)
{
	import std.range : chain;
	import std.conv : to;
	d.command(chain("play"," ",pos.to!string));
}
void playById(Daemon d, int id)
{
	import std.range : chain;
	import std.conv : to;
	d.command(chain("playid"," ",id.to!string));
}
void pause(Daemon d)
{
	import std.range : chain;
	d.command(chain("pause"," 1"));
}
void resume(Daemon d)
{
	import std.range : chain;
	d.command(chain("pause"," 0"));
}
void stop(Daemon d)
{
	d.command("stop");
}
void repeat(Daemon d, bool repeat = true)
{
	if (repeat)
		d.command("repeat 1");
	else
		d.command("repeat 0");
}
void random(Daemon d, bool random = true)
{
	if (random)
		d.command("random 1");
	else
		d.command("random 0");
}
void setVolume(Daemon d, int volume)
{
	import std.range : chain;
	import std.conv : to;
	enforce(volume >= 0 && volume <= 100);
	d.command(chain("setvol"," ",volume.to!string));
}
auto playListInfo(Daemon d, int start = -1, int end = -1)
{
	import std.range : chain;
	import std.conv : to;
	char[] r;
	if (start > -1 && end > -1)
		r = d.command(chain("playlistinfo"," ",start.to!string,":",end.to!string));
	else if (start > -1)
		r = d.command(chain("playlistinfo"," ",start.to!string));
	else
		r = d.command("playlistinfo");
	return r.deserialize!(Song[]);
}
auto clearPlaylist(Daemon d)
{
	d.command("clear");
}
auto remove(Daemon d, int start, int end = -1)
{
	import std.range : chain;
	import std.conv : to;
	if (end > -1)
		d.command(chain("delete"," ",start.to!string,":",end.to!string));
	else
		d.command(chain("delete"," ",start.to!string));
}
auto deleteId(Daemon d, int id)
{
	import std.range : chain;
	import std.conv : to;
	d.command(chain("deleteid"," ",id.to!string));
}
struct Position
{
	@Name("Id") int id;
}
auto add(Daemon d, string uri)
{
	import std.range : chain;
	return d.command(chain("addid ",uri)).deserialize!Song;
}
struct Job
{
	@Name("updating_db") int id;
}
auto update(Daemon d, string uri = "")
{
	import std.range : chain;
	if (uri.length > 0)
		return d.command(chain("update"," ",uri)).deserialize!Job;
	return d.command("update").deserialize!Job;
}
auto rescan(Daemon d, string uri = "")
{
	import std.range : chain;
	if (uri.length > 0)
		return d.command(chain("rescan"," ",uri)).deserialize!Job;
	return d.command("rescan").deserialize!Job;
}
@Delimiter("outputid") struct Output
{
	@Name("outputid") int id;
	@Name("outputname") string name;
	@Name("outputenabled") int enabled;
}
auto outputs(Daemon d)
{
	return d.command("outputs").deserialize!(Output[]);
}
auto disableOutput(Daemon d, int id)
{
	import std.range : chain;
	import std.conv : to;
	d.command(chain("disableoutput ",id.to!string));
}
auto enableOutput(Daemon d, int id)
{
	import std.range : chain;
	import std.conv : to;
	d.command(chain("enableoutput ",id.to!string));
}
enum Tag
{
	Artist,
	Genre
}
import std.stdio;
auto count(Daemon d, Tag tag, string group = "", string groupType = "")
{
	import std.range : chain;
	import std.conv : to;
	auto r = d.command(chain("count"," ",tag.to!string));
	writeln(r);
}
auto tagTypes(Daemon d)
{
	import std.range : chain;
	auto r = d.command("tagtypes");
	writeln(r);
}
auto list(Output)(Daemon d, string params)
{
	import std.range : chain;
	return d.command(chain("list"," ",params)).deserialize!Output;
}
auto find(Output)(Daemon d, string params)
{
	import std.range : chain;
	return d.command(chain("find"," ",params)).deserialize!Output;
}
auto findAdd(Daemon d, string params)
{
	import std.range : chain;
	d.command(chain("findadd"," ",params));
}