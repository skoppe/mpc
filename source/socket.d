module mpcd.socket;

import vibe.core.net;
import vibe.core.core : sleep, Tid, runTask;
import core.sys.posix.netinet.in_;
import core.time : Duration;

struct Socket
{
	private { 
		TCPConnection con; 
		ubyte[2048] buffer;
	}
	~this()
	{
		close();
	}
	size_t bufferSize()
	{
		return buffer.length;
	}
	void open(string host, ushort port)
	{
		if (con)
			con.close();
		con = connectTCP(host,port);
		assert(con.connected);
	}
	auto connected()
	{
		return con.connected;
	}
	void close()
	{
		if (con !is null)
			con.close();
		con = null;
	}
	void send(const(char[]) data)
	{
		assert(con !is null);
		con.write(data);
	}
	bool hasData()
	{
		assert(con !is null);
		return con.dataAvailableForRead();
	}
	bool waitForData(Duration timeout)
	{
		return con.waitForData(timeout);
	}
	char[] read()
	{
		import std.algorithm : min;
		assert(con !is null);
		uint size = min(cast(uint)con.leastSize,cast(uint)bufferSize);
		con.read(buffer[0..size]);
		return cast(char[])buffer[0..size];
	}
}