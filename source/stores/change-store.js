import RxDom from 'rx-dom';
import Rx from 'rx';

let changeSubject;
let connectionSubject;
let retryCnt = 1;

function openWebSocket(url, subject)
{
	subject = subject || new Rx.Subject();
	let ws = new WebSocket(url);
	function clean()
	{
		ws.onopen = undefined;
		ws.onclose = undefined;
		ws.onmessage = undefined;
		ws.close();
	}
	ws.onopen = ()=>{
		subject.onNext({type: 'connection', active: true});
		retryCnt = 1;
	}
	ws.onclose = ()=>{
		clean();
		subject.onNext({type: 'connection', active: false});
		setTimeout(()=>{openWebSocket(url, subject)},1000);
	}
	ws.onmessage = (msg)=>{
		subject.onNext({type: 'data', data: msg.data});
	}
	ws.onerror = ()=>{
		clean();
		subject.onNext({type: 'connection', active: false});
		let time = Math.min(10000,(retryCnt++)*1000);
		setTimeout(()=>{openWebSocket(url, subject)},time);
	}
	return subject;
}

function setupChangeStream()
{
	if (changeSubject != null)
		return;

	let subject = openWebSocket("ws://"+window.location.host+"/ws/v1");
	changeSubject = subject.filter(d=>d.type=='data').map(d=>d.data);
	connectionSubject = subject.filter(d=>d.type=='connection').map(d=>d.active);
}

module.exports={
	changes:
	{
		subscribe: function(...args)
		{
			setupChangeStream();
			return changeSubject.subscribe(...args);
		},
		get: function()
		{
			setupChangeStream();
			return changeSubject;
		}
	},
	status:
	{
		subscribe: function(...args)
		{
			setupChangeStream();
			return connectionSubject.subscribe(...args);
		},
		get: function()
		{
			setupChangeStream();
			return connectionSubject;
		}
	}
}