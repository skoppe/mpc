import RxDom from 'rx-dom';

module.exports={
	getArtists: function(params)
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/artist?query="+encodeURIComponent(JSON.stringify(params)),
			method:"GET",
			responseType:"json"
		}).pluck('response');
	},
	getArtist: function(name)
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/artist/"+encodeURIComponent(name),
			method:"GET",
			responseType:"json"
		}).pluck('response');
	}
}