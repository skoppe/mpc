import RxDom from 'rx-dom';

module.exports={
	getGenres: function()
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/genre",
			method:"GET",
			responseType:"json"
		}).pluck('response');
	},
	getArtistsByGenre: function(name)
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/genre/"+encodeURIComponent(name)+"/artist",
			method:"GET",
			responseType:"json"
		}).pluck('response');
	},
	getAlbumByGenre: function(name)
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/genre/"+encodeURIComponent(name)+"/album",
			method:"GET",
			responseType:"json"
		}).pluck('response');
	}
}