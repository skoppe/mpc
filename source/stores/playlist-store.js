import RxDom from 'rx-dom';
import Rx from 'rx';
import ChangeStore from './change-store.js'

let playlistSubject;

function setupPlaylistStream()
{
	if (playlistSubject != null)
		return;
	playlistSubject = new Rx.ReplaySubject(1);
	queryPlayList();
}
function queryPlayList()
{
	RxDom.DOM.ajax(
	{
		url:"/api/v1/playlist",
		method:"GET",
		responseType:"json"
	}).pluck('response').doOnNext(r=>playlistSubject.onNext(r)).subscribe();
}
ChangeStore.changes.get().filter(event=>event=="playlist").subscribe(queryPlayList);

module.exports={
	subscribe: function(...args)
	{
		setupPlaylistStream();
		return playlistSubject.subscribe(...args);
	},
	update: function(status)
	{
		
	}
}