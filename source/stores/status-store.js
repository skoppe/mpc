import RxDom from 'rx-dom';
import Rx from 'rx';
import ChangeStore from './change-store.js'

let statusSubject;

function setupStatusStream()
{
	if (statusSubject != null)
		return;
	statusSubject = new Rx.ReplaySubject(1);
	queryStatus();
}
function queryStatus()
{
	RxDom.DOM.ajax(
	{
		url:"/api/v1/status",
		method:"GET",
		responseType:"json"
	}).pluck('response').doOnNext(r=>statusSubject.onNext(r)).subscribe();
}
ChangeStore.changes.get().filter(event=>event=="player"||event=="mixer"||event=="options").subscribe(queryStatus);

module.exports={
	subscribe: function(...args)
	{
		setupStatusStream();
		return statusSubject.subscribe(...args);
	},
	update: function(status)
	{
		
	}
}