import RxDom from 'rx-dom';

module.exports={
	getAlbums: function(params)
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/album?query="+encodeURIComponent(JSON.stringify(params)),
			method:"GET",
			responseType:"json"
		}).pluck('response');
	},
	getAlbum: function(name)
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/album/"+encodeURIComponent(name),
			method:"GET",
			responseType:"json"
		}).pluck('response');
	}
}