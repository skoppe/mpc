import RxDom from 'rx-dom';

module.exports={
	play: function(name)
	{
		RxDom.DOM.ajax(
		{
			url:"/api/v1/playlist/album/"+encodeURIComponent(name),
			method:"POST",
			responseType:"json",
			body:JSON.stringify({mode:"replace"}),
			headers:{"Content-Type":"application/json"}
		}).subscribe();
	},
	enqueue: function(name)
	{
		RxDom.DOM.ajax(
		{
			url:"/api/v1/playlist/album/"+encodeURIComponent(name),
			method:"POST",
			responseType:"json",
			body:JSON.stringify({mode:"add"}),
			headers:{"Content-Type":"application/json"}
		}).subscribe();
	}
}