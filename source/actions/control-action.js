import RxDom from 'rx-dom';
import Rx from 'rx';

let volumeSubject = new Rx.Subject();

let ajax = (url,method,body)=>{
	let opts = {
		url:url,
		method:method,
		responseType:"json"		
	}
	if (method != "GET" && body != undefined)
	{
		opts.body = JSON.stringify(body);
		opts.headers = {"Content-Type":"application/json"};
	}
	return RxDom.DOM.ajax(opts);
}
let post = (url,body)=>{
	return ajax(url,"POST",body);
}
let put = (url,body)=>{
	return ajax(url,"PUT",body);
}
let playback = (p)=>{
	return post("/api/v1/playback",{playback:p}).pluck('response');
}
module.exports={
	status: function()
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/status",
			method:"GET",
			responseType:"json"
		}).pluck('response');
	},
	volume:
	{
		set: (vol)=>{ return post("/api/v1/volume",{volume:vol}).pluck('response').doOnNext(vol=>volumeSubject.onNext(vol)); },
		subscribe: (...args)=>{ return volumeSubject.subscribe(...args); }
	},
	previous: function()
	{
		return playback("previous");
	},
	play: function(song)
	{
		if (song == undefined)
			return playback("play");
		return put("/api/v1/playlist/song/"+song).pluck('response');
	},
	stop: function()
	{
		return playback("stop");
	},
	pause: function()
	{
		return playback("pause");
	},
	resume: function()
	{
		return playback("resume");
	},
	next: function()
	{
		return playback("next");
	},
	repeat: function(state)
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/repeat",
			method: state == true ? "PUT" : "DELETE",
			responseType:"json"
		}).pluck('response');
	},
	random: function(state)
	{
		return RxDom.DOM.ajax(
		{
			url:"/api/v1/random",
			method: state == true ? "PUT" : "DELETE",
			responseType:"json"
		}).pluck('response');
	}
}