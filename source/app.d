import mpcd.mpd;

import vibe.core.core;
import vibe.http.server : HTTPServerSettings;
import vibe.http.router : URLRouter;
import vibe.data.serialization : optional;
import vibe.web.rest;

@Delimiter("Album") struct Album
{
	@Name("Album") string name;
}
@Delimiter("Artist") struct Artist
{
	@Name("Artist") string name;
}
@Delimiter("Genre") struct Genre
{
	@Name("Genre") string name;
}
struct Query
{
	@optional string genre;
	@optional string artist;
	@optional string album;
}
struct StatusResponse
{
	Status status;
	Song current;
	Song next;
}

@path("/api/v1")
interface MpdApiInterface1
{
	Album[] getAlbum(Query query);
	Artist[] getArtist(Query query);
	@path("album/:name") Song[] getAlbum(string _name);
	@path("artist/:name") Album[] getArtist(string _name);
	@path("playlist/album/:name") @method(HTTPMethod.POST)
	void playAlbum(string _name, string mode);
	@path("playlist")
	Song[] getPlayList();
	@path("playlist/song/:pos")
	void putSong(int _pos);
	StatusResponse getStatus();
	@path("volume") @method(HTTPMethod.POST)
	void setVolume(int volume);
	@path("playback") @method(HTTPMethod.POST)
	void setPlayback(string playback);
	void setRepeat();
	void removeRepeat();
	void setRandom();
	void removeRandom();
	Genre[] getGenre();
	@path("genre/:name/artist") Artist[] getArtistByGenre(string _name);
	@path("genre/:name/album") Album[] getAlbumByGenre(string _name);
}
auto generateMpdQuery(Query q)
{
	import std.array : appender;
	import std.format : formattedWrite;
	auto app = appender!string;
	if (q.genre != string.init)
		app.formattedWrite("genre \"%s\"",q.genre);
	if (q.artist != string.init)
		app.formattedWrite("artist \"%s\"",q.artist);
	if (q.album != string.init)
		app.formattedWrite("album \"%s\"",q.album);
	return app.data;
}
auto executeQuery(Result, alias type)(Daemon d, Query query)
{
	import std.range : chain;
	import std.conv : text;
	__traits(getMember, query, type) = "";
	return d.list!Result(chain(type," ",query.generateMpdQuery()).text());	
}
class MpdApi1 : MpdApiInterface1
{
	private {
		Daemon daemon;
	}
	this(Daemon d)
	{
		daemon = d;
	}
	override:
		StatusResponse getStatus()
		{
			auto s = StatusResponse(daemon.status());
			if (s.status.song != -1)
			{
				auto ss = daemon.playListInfo(s.status.song);
				if (!ss.empty)
					s.current = ss.front();
			}
			if (s.status.nextsong != -1)
			{
				auto ss =daemon.playListInfo(s.status.nextsong);
				if (!ss.empty)
					s.next = ss.front();
			}
			return s;
		}
		void setVolume(int volume)
		{
			daemon.setVolume(volume);
		}
		void setPlayback(string playback)
		{
			switch(playback)
			{
				default: return;
				case "play": daemon.play(0); break;
				case "resume": daemon.resume(); break;
				case "pause": daemon.pause(); break;
				case "next": daemon.next(); break;
				case "previous": daemon.previous(); break;
			}
		}
		void putSong(int _pos)
		{
			daemon.play(_pos);
		}
		Album[] getAlbum(Query query)
		{
			return daemon.executeQuery!(Album[],"album")(query);
		}
		Artist[] getArtist(Query query)
		{
			return daemon.executeQuery!(Artist[],"artist")(query);
		}
		Song[] getAlbum(string _name)
		{
			return daemon.find!(Song[])("album \""~_name~"\"");
		}
		Album[] getArtist(string _name)
		{
			return daemon.list!(Album[])("album artist \""~_name~"\"");
		}
		void playAlbum(string _name, string mode)
		{
			if (mode == "replace")
				daemon.clearPlaylist();
			daemon.findAdd("album \""~_name~"\"");
			if (mode == "replace")
				daemon.play(0);
		}
		Song[] getPlayList()
		{
			return daemon.playListInfo();
		}
		void setRepeat()
		{
			daemon.repeat(true);
		}
		void removeRepeat()
		{
			daemon.repeat(false);
		}
		void setRandom()
		{
			daemon.random(true);
		}
		void removeRandom()
		{
			daemon.random(false);
		}
		Genre[] getGenre()
		{
			return daemon.list!(Genre[])("genre");
		}
		Artist[] getArtistByGenre(string _name)
		{
			return daemon.list!(Artist[])("artist genre \""~_name~"\"");
		}
		Album[] getAlbumByGenre(string _name)
		{
			return daemon.list!(Album[])("album genre \""~_name~"\"");
		}
}
void startMpdApi(Daemon daemon, URLRouter router)
{
	registerRestInterface(router, new MpdApi1(daemon));
}
void startWebserver(HTTPServerSettings serverSettings, URLRouter router)
{
	import vibe.core.core : runEventLoop;
	import vibe.http.router : URLRouter;
	import vibe.core.log : logInfo;
	import vibe.http.server : listenHTTP, HTTPServerRequest, HTTPServerResponse;
	import vibe.http.fileserver : serveStaticFiles, HTTPFileServerSettings;

	auto fsettings = new HTTPFileServerSettings;
	import std.datetime : seconds;
	fsettings.maxAge = 31536000.seconds;
	router.get("/assets/*", serveStaticFiles("./public", fsettings));
	router.get("/*", (scope HTTPServerRequest req, scope HTTPServerResponse res)
	{
		res.headers["Content-Type"] = "text/html";
		import vibe.http.server : render;
		import std.file : readText;
		auto css = "/assets/css/"~readText("./bundle.css.conf");
		auto js = "/assets/js/"~readText("./bundle.js.conf");
		res.render!("index.dt",css,js);
	});

	listenHTTP(serverSettings, router);
	logInfo("Server Started...");
}
import vibe.stream.stdio : StdinStream;
auto byLine(StdinStream stream)
{
	struct byLineImpl
	{
		private
		{
			char[128] buf;
			StdinStream stream;
			char[] slice;
		}
		this(StdinStream s)
		{
			stream = s;
		}
		@property bool empty()
		{
			if (slice.length != 0)
				return false;
			if (!stream.connected())
				return false;
			return !fetchData();
		}
		private bool fetchData()
		{
			import core.time : dur;
			import std.stdio;
			if (!stream.waitForData(dur!"seconds"(0)))
				return false;
			uint offset = 0;
			uint size = 0;
			do
			{
				size = min(cast(uint)stream.leastSize,(cast(uint)buf.length)-offset);
				if (size == 0)
					return false;
				stream.read(cast(ubyte[])buf[offset..size+offset]);
				if (buf[size+offset-1] == '\n')
				{
					slice = buf[0..size+offset-2];
					return true;
				}
				offset += size;
			} while (buf.length > offset);
			import std.array : appender;
			auto a = appender!(char[]);
			a.put(buf[]);
			do
			{
				if (!stream.waitForData(dur!"seconds"(0)))
					return false;
				size = min(cast(uint)stream.leastSize,cast(uint)buf.length);
				if (size == 0)
					return false;
				stream.read(cast(ubyte[])buf[0..size]);
				a.put(buf[0..size]);
				if (buf[size-1] == '\n')
				{
					slice = a.data[0..$-2];
					return true;
				}
			} while(true);
			assert(0);
		}
		char[] front(){
			if (slice.length == 0)
				fetchData();
			return slice;
		}
		void popFront(){
			slice.length = 0;
		}
	}
	return byLineImpl(stream);
}
void startCliInterface(Daemon daemon)
{
	runTask({
		import std.stdio : readln, write, writeln, writefln;
		auto stdin = new StdinStream();
		char[1024] buf;
		foreach(line; stdin.byLine)
		{
			try
			{
				writeln(daemon.command(line));
			} catch (DaemonError err)
			{
				writefln("Error %s: %s",err.code,err.msg);
			}
		}
	});
}
struct Settings
{
	string mpdHost;
	ushort mpdPort = 6600;
	ushort port = 9934;
}
auto readSettings()
{
	import vibe.core.args : readOption;
	import std.process : environment;
	import std.conv : to;
	string mpdHost = environment.get("MPD_HOST", "localhost");
	ushort mpdPort = environment.get("MPD_PORT", "6600").to!ushort;
	ushort port = environment.get("MPC_PORT", "9934").to!ushort;
	auto set = Settings(mpdHost,mpdPort,port);
	readOption(
		"mpdhost",
		&set.mpdHost,
		"MPD Host (defaults to \"localhost\")"
	);
	readOption(
		"mpdport",
		&set.mpdPort,
		"Domain (defaults to 6600)"
	);
	readOption(
		"port",
		&set.port,
		"Listening Port (default to 9934)"
	);
	return set;
}
auto createServerSettings(Settings settings)
{
	auto ss = new HTTPServerSettings;
	ss.port = settings.port;
	ss.bindAddresses = ["0.0.0.0"];
	ss.useCompressionIfPossible = true;
	return ss;
}
import vibe.http.websockets : WebSocket, handleWebSockets;

void main()
{
	import vibe.core.args : finalizeCommandLineOptions;
	auto settings = readSettings();

	if (!finalizeCommandLineOptions())
		return;

	auto serverSettings = createServerSettings(settings);
	auto router = new URLRouter;
	auto daemon = new Daemon(settings.mpdHost,settings.mpdPort);
	auto ws = new DaemonWebSocket(new Daemon(settings.mpdHost,settings.mpdPort));
	router.get("/ws/v1", handleWebSockets(&ws.handleWebSocketConnection));

	startCliInterface(daemon);
	startMpdApi(daemon, router);
	startWebserver(serverSettings, router);
	runEventLoop();
}
class DaemonWebSocket
{
	private{
		Daemon daemon;
	}
	this(Daemon d)
	{
		daemon = d;
	}
	void handleWebSocketConnection(scope WebSocket socket)
	{
		import std.stdio;
		import std.datetime : seconds;
		bool completed = false;
		auto sub = daemon.idle.subscribe((status){
			socket.send(status.to!string);
		},null,(){ completed = true; });

		while (socket.connected && !completed)
			sleep(1.seconds);
		sub.dispose();
	}
}
